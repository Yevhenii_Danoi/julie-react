const functions = require("firebase-functions");
const fetch = require("node-fetch");
const { v4 } = require("uuid");
const admin = require("firebase-admin");

// import * as functions from "firebase-functions";
// import fetch from "node-fetch";
// import { v4 } from "uuid";

// import admin from "firebase-admin";

admin.initializeApp();

const monthsNamesInDeclension = [
  "января",
  "февраля",
  "марта",
  "апреля",
  "мая",
  "июня",
  "июля",
  "августа",
  "сентября",
  "октября",
  "ноября",
  "декабря",
];

const isToday = (someDate) => {
  const today = new Date();
  return (
    someDate.getDate() == today.getDate() &&
    someDate.getMonth() == today.getMonth() &&
    someDate.getFullYear() == today.getFullYear()
  );
};

exports.newOnlineRecording = functions.firestore
  .document("records/{recordId}")
  .onCreate(async (snap, context) => {
    await admin
      .firestore()
      .collection("records")
      .doc(context.params.recordId)
      .get()
      .then(async (recordSnapshot) => {
        await admin
          .firestore()
          .collection("users")
          .doc(recordSnapshot.data().masterID)
          .get()
          .then(async (docSnap) => {
            if (docSnap.exists) {
              if (!recordSnapshot.data().clientID)
                await admin
                  .firestore()
                  .collection("users")
                  .doc(recordSnapshot.data().masterID)
                  .collection("clients")
                  .doc(context.params.recordId)
                  .set({
                    avatar: "null",
                    lastVisit: "не был",
                    name: recordSnapshot.data().fullName,
                    phoneNumber: recordSnapshot.data().phoneNumber,
                    color: recordSnapshot.data().color,
                  });

              await admin
                .firestore()
                .collection("users")
                .doc(recordSnapshot.data().masterID)
                .collection("records")
                .add({
                  lastModified: recordSnapshot.data().lastModified,
                  fromOnline: true,
                  areaID: recordSnapshot.data().areaID,
                  procedureID: recordSnapshot.data().procedureID,
                  startTime: recordSnapshot.data().startTime,
                  endTime: recordSnapshot.data().endTime,
                  clientID: recordSnapshot.data().clientID
                    ? recordSnapshot.data().clientID
                    : context.params.recordId,
                  isDone: false,
                  date: recordSnapshot.data().date,
                  isNewClient: recordSnapshot.data().clientID ? false : true,
                });
              await admin
                .firestore()
                .collection("records")
                .doc(context.params.recordId)
                .delete();
            }
          });
      });
    return;
  });

exports.notifyAboutOnlineRecord = functions.firestore
  .document("/users/{userId}/records/{recordId}")
  .onCreate((_snap, context) => {
    let allExpoTokens = {};
    let activeExpoTokens = [];
    let docId = v4();
    const records = [];

    return admin
      .firestore()
      .collection("users")
      .doc(context.params.userId)
      .get()
      .then((userSnapshot) => {
        allExpoTokens = userSnapshot.data().expoTokens;

        if (Object.keys(allExpoTokens).length > 0) {
          Object.keys(allExpoTokens).filter((token) => {
            if (allExpoTokens[token] === true) {
              activeExpoTokens.push(`ExponentPushToken[${token}]`);
            }
          });
        }
        if (activeExpoTokens)
          return admin
            .firestore()
            .collection("users")
            .doc(context.params.userId)
            .collection("records")
            .doc(context.params.recordId)
            .get()
            .then(async (recordSnapshot) => {
              if (recordSnapshot.data().fromOnline) {
                let body = "";
                let newClient = "";
                let clientName = "";
                let seperateDate = recordSnapshot.data().date.split("-");

                if (recordSnapshot.data().isNewClient) {
                  newClient = "Новый клиент!";
                }
                await admin
                  .firestore()
                  .collection("users")
                  .doc(context.params.userId)
                  .collection("clients")
                  .doc(recordSnapshot.data().clientID)
                  .get()
                  .then((clientSnap) => {
                    clientName = clientSnap.data().name;
                  });

                if (isToday(new Date(recordSnapshot.data().date)))
                  body = `Ждите сегодня клиента! ${newClient} ${clientName} записался(ась) к вам на процедуру`;
                else
                  body = `${newClient} ${clientName} записался(ась) онлайн на ${
                    seperateDate[2]
                  } ${monthsNamesInDeclension[seperateDate[1] - 1]}`;

                records.push({
                  to: activeExpoTokens,
                  title: "Julie | Новая запись",
                  body: body,
                  data: { docId: docId },
                });
              }

              return Promise.all(records);
            });
      })
      .then((records) => {
        if (records && records.length > 0) {
          functions.logger.log("New record", records);
          fetch("https://exp.host/--/api/v2/push/send", {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
            },
            body: JSON.stringify(records),
          });
          admin
            .firestore()
            .collection("users")
            .doc(context.params.userId)
            .collection("notifications")
            .doc(docId)
            .set({
              message: records[0].body,
              date: new Date(),
              isViewed: false,
            });
        } else {
          functions.logger.log(
            "It was a regular recording. Look! Record data: ",
            records
          );
        }
      });
  });
