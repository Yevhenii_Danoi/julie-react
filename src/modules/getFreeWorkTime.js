import dayjs from "dayjs";

const dateCercion = (date, leadingDate) => {
  const dateVal = dayjs(leadingDate).toISOString().split("T", 1)[0].split("-");
  date.setYear(dateVal[0]);
  date.setMonth(dateVal[1] - 1);
  date.setDate(dateVal[2]);
  date.setSeconds(0);

  return date;
};

export default dateCercion;
