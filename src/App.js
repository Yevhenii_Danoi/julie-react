import logo from "./logo.svg";
import "./App.css";
import RecordForm from "./components/RecordForm";

function App() {
  return (
    <div className="App">
      <RecordForm />
    </div>
  );
}

export default App;
