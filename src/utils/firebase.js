import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyADk5PHI3neGHaNwEHbP0R9OLw-uLGcJCY",
  authDomain: "julie-614ba.firebaseapp.com",
  projectId: "julie-614ba",
  storageBucket: "julie-614ba.appspot.com",
  messagingSenderId: "34131280099",
  appId: "1:34131280099:web:c681ef114f418ede2185d6",
  measurementId: "G-8M04YSMFNK",
};

const app = initializeApp(firebaseConfig);

export const db = getFirestore(app);

export default app;
