import React, { useEffect, useState } from "react";
import { Avatar } from "@mui/material";
import { StyledEngineProvider } from "@mui/material/styles";
import { doc, getDoc } from "firebase/firestore";
import { db } from "../utils/firebase";
import { useParams } from "react-router-dom";

export default function Success({ content }) {
  var headID = document.getElementsByTagName("head")[0];
  var link = document.createElement("link");
  link.rel = "icon";

  headID.appendChild(link);

  link.href = "%PUBLIC_URL%/favicon.ico";

  const [masterID, setMasterID] = useState(null);
  const [initials, setInitials] = useState("");
  const [photo, setPhoto] = useState(null);

  let params = useParams();

  useEffect(() => {
    (async () => {
      if (masterID) {
        const ref = doc(db, `users/${masterID}`);
        const docPersonalDataRef = await getDoc(ref);

        document.querySelector(".subheader").innerHTML =
          docPersonalDataRef.data().name +
          " " +
          docPersonalDataRef.data().surname;

        if (docPersonalDataRef.data().profilePhoto) {
          setPhoto(docPersonalDataRef.data().profilePhoto);
        } else {
          let init = "";
          const name = docPersonalDataRef.data().name[0];
          const surname = docPersonalDataRef.data().surname[0];
          if (name) init += name;
          if (surname) init += surname;
          setInitials(init);
        }
      }
    })();
  }, [masterID]);

  useEffect(() => {
    setMasterID(params.masterId);
  }, [params]);

  return (
    <React.Fragment>
      <StyledEngineProvider injectFirst>
        <div className="container">
          <div className="contentContainer">
            <div className="overTheForm">
              <div className="overTheFormSuperimposition"></div>
              <div className="headerContainer">
                <div className="headerText">
                  <span className="header">Онлайн-запись</span>
                  <span className="subheader"></span>
                </div>
                <div className="avatarContainer">
                  {initials.length <= 0 ? (
                    <Avatar className="avatarStyle" src={photo} />
                  ) : (
                    <Avatar className="avatarStyle">{initials}</Avatar>
                  )}
                </div>
              </div>
            </div>
            <div className="formContainer">
              <div className="form">{content}</div>
            </div>
          </div>
          <footer>
            <p className="footerText">Julie {new Date().getFullYear()}</p>
          </footer>
        </div>
      </StyledEngineProvider>
    </React.Fragment>
  );
}
