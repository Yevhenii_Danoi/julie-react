import React, { useState, useReducer, useEffect } from "react";
import { useParams } from "react-router-dom";

import FormRecordDetails from "./FormRecordDetails";
import FormUserDetails from "./FormUserDetails";
import Success from "./Success";
import NotFound from "./NotFound";
import $ from "jquery";

import { doc, getDoc } from "firebase/firestore";
import { db } from "../utils/firebase";
import FormUserClientCode from "./FormUserClientCode";

const formReducer = (state, event) => {
  return {
    ...state,
    [event.name]: event.value,
  };
};

export default function RecordForm() {
  const [masterID, setMasterID] = useState(null);

  let params = useParams();

  const [formData, setFormData] = useReducer(formReducer, {
    isNoCode: false,
    clientCode: "",
    fullName: "",
    phone: "",
    date: new Date(),
    areaObject: null,
    time: "",
  });
  useEffect(() => {
    setMasterID(params.masterId);
  }, [params]);

  const [state, setState] = useState({
    step: 1,
  });

  const nextStep = (stepParams = 1) => {
    const { step } = state;
    setState({
      step: step + stepParams,
    });
  };

  const prevStep = (stepParams = 1) => {
    const { step } = state;
    setState({
      step: step - stepParams,
    });
  };

  const handleChange = (event) => {
    setFormData({
      name: event.target.name,
      value: event.target.value,
    });
  };

  const { step } = state;
  const { fullName, phone, date, areaObject, time, clientCode, isNoCode } =
    formData;
  const values = {
    fullName,
    phone,
    date,
    areaObject,
    time,
    clientCode,
    isNoCode,
  };

  switch (step) {
    case 1:
      return (
        <FormUserClientCode
          nextStep={nextStep}
          handleChange={handleChange}
          values={values}
          db={db}
          masterID={masterID}
        />
      );
    case 2:
      return (
        <FormUserDetails
          nextStep={nextStep}
          prevStep={prevStep}
          handleChange={handleChange}
          values={values}
        />
      );
    case 3:
      return (
        <FormRecordDetails
          nextStep={nextStep}
          prevStep={prevStep}
          handleChange={handleChange}
          values={values}
          db={db}
          masterID={masterID}
        />
      );
    case 4:
      return <Success values={values} />;
    default:
      return <NotFound />;
  }
}
