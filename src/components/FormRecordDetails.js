import React, { useState, useEffect } from "react";
import {
  Autocomplete,
  TextField,
  CircularProgress,
  Backdrop,
} from "@mui/material";
import dayjs from "dayjs";
import randomColor from "randomcolor";

import {
  onSnapshot,
  collection,
  getDocs,
  query,
  where,
  addDoc,
} from "firebase/firestore";

import { DatePicker } from "react-nice-dates";
import "react-nice-dates/build/style.css";
import { ru } from "date-fns/locale";
import { getDay } from "date-fns";

import dateCercion from "../modules/getFreeWorkTime";
import "../styles/style.css";

export default function FormRecordDetails({
  values,
  handleChange,
  nextStep,
  prevStep,
  db,
  masterID,
}) {
  const [disabled, setDisabled] = useState(true);

  const [openAreas, setOpenAreas] = useState(false);
  const [openTimeSlots, setOpenTimeSlots] = useState(false);
  const [submitting, setSubmitting] = useState(false);

  const [isCounting, setIsCounting] = useState(undefined);
  const [isCounted, setIsCounted] = useState(undefined);

  const [areas, setAreas] = useState([]);
  const [timeSlots, setTimeSlots] = useState([]);
  const [startOfDayTime, setStartOfDayTime] = useState(undefined);
  const [endOfDayTime, setEndOfDayTime] = useState(undefined);
  const [hasWorkTimeSet, setHasWorkTimeSet] = useState(false);
  const [hasLunchTimeSet, setHasLunchTimeSet] = useState(false);
  const [startLunchTime, setStartLunchTime] = useState(undefined);
  const [endLunchTime, setEndLunchTime] = useState(undefined);
  const [allTimesBetweenEntries, setAllTimesBetweenEntries] =
    useState(undefined);

  const [selectedDate, setSelectedDate] = useState(values.date);

  const [isDateSelected, setIsDateSelected] = useState(false);

  const [isTrackedChanges, setIsTrackedChanges] = useState(false);
  const [startTimeDataTracking, setStartTimeDataTracking] = useState(null);

  const [disableDays, setDisableDays] = useState([]);

  const [smallestTimeInterval, setSmallestTimeInterval] = useState(undefined);

  const [possibleTimesBetweenEntries, setPossibleTimesBetweenEntries] =
    useState(undefined);

  const isDateDisabled = !values.areaObject;
  const areaLoading = masterID && openAreas && areas.length === 0;
  const timeLoading = openTimeSlots && isCounted === false;

  const modifiers = {
    disabled: (date) => disableDays.some((day) => getDay(date) === day),
  };

  const modifiersClassNames = {
    highlight: "-highlight",
  };

  const Continue = (e) => {
    e.preventDefault();
    handleCreateRecordInDatabase();
  };

  const back = (e) => {
    e.preventDefault();

    const stepParams = values.isNoCode ? 1 : 2;
    if (stepParams === 2) {
      handleChange({
        target: {
          name: "fullName",
          value: "",
        },
      });
      handleChange({
        target: {
          name: "phone",
          value: "",
        },
      });
    }
    handleChange({
      target: {
        name: "areaObject",
        value: null,
      },
    });
    handleChange({
      target: {
        name: "time",
        value: "",
      },
    });
    handleChange({
      target: {
        name: "date",
        value: new Date(),
      },
    });

    prevStep(stepParams);
  };

  useEffect(() => {
    if (values.time && values.time.length > 0) {
      setDisabled(false);
    } else {
      setDisabled(true);
    }
  }, [values.time]);

  useEffect(() => {
    if (values.areaObject) onDateChange(selectedDate);
  }, [disableDays]);

  useEffect(() => {
    if (isCounting) {
      setIsCounted(false);
      setIsTrackedChanges(false);
    } else {
      setIsCounted(true);
    }
  }, [isCounting]);

  useEffect(() => {
    if (isCounted) {
      if (timeSlots.length > 0 && !isTrackedChanges) {
        setStartTimeDataTracking(new Date());
        setIsTrackedChanges(true);
      } else if (timeSlots.length <= 0 && isTrackedChanges) {
        setTimeSlots([
          `Нет свободных мест для '${values.areaObject.procedure}`,
        ]);
      }
    }
  }, [isCounted]);

  useEffect(() => {
    let active = true;
    if (isTrackedChanges) {
      const onlineRecordsQuery = query(
        collection(db, `records`),
        where("date", "==", dayjs(selectedDate).toISOString().split("T", 1)[0])
      );

      onSnapshot(onlineRecordsQuery, (snapshot) => {
        snapshot.docChanges().forEach((change) => {
          if (change.type === "added") {
            if (
              change.doc.data().lastModified.seconds * 1000 >=
                startTimeDataTracking.getTime() &&
              active
            ) {
              if (!submitting) {
                handleChange({
                  target: {
                    name: "time",
                    value: "",
                  },
                });
                setIsDateSelected(true);
              }
            }
          }
        });
      });
      const regularRecordsQuery = query(
        collection(db, `users/${masterID}/records`),
        where("date", "==", dayjs(selectedDate).toISOString().split("T", 1)[0])
      );
      onSnapshot(regularRecordsQuery, (snapshot) => {
        snapshot.docChanges().forEach((change) => {
          if (change.type !== "modified") {
            if (
              (change.type === "removed" ||
                change.doc.data().lastModified.seconds * 1000 >=
                  startTimeDataTracking.getTime()) &&
              active
            ) {
              if (!submitting) {
                handleChange({
                  target: {
                    name: "time",
                    value: "",
                  },
                });
                setIsDateSelected(true);
              }
            }
          }
        });
      });
    }
    return () => {
      active = false;
    };
  }, [isTrackedChanges, submitting]);

  useEffect(() => {
    if (values.areaObject && values.areaObject.duration) {
      onDateChange(selectedDate);
    }
  }, [values.areaObject]);

  useEffect(() => {
    const fetchData = async () => {
      if (masterID) {
        const docScheduleRef = query(
          collection(db, `users/${masterID}/schedule`)
        );
        onSnapshot(docScheduleRef, (snapshot) => {
          let weekendsDays = {};
          snapshot.forEach((item) => {
            if (item.data().subtitle === "Выходной") {
              weekendsDays[item.data().dayID] = item.data().day;
            }
          });
          setDisableDays(Object.keys(weekendsDays).map((s) => +s));
        });
      }
    };
    fetchData();
  }, [db, masterID]);

  //LOADING AREAS
  useEffect(() => {
    let active = true;

    if (!areaLoading) {
      return undefined;
    }

    if (masterID) {
      (async () => {
        const areasSnapshot = await getDocs(
          collection(db, `users/${masterID}/areas`)
        );
        const getPriceList = async (area) => {
          let priceListData = [];
          const dbPriceList = await getDocs(
            collection(db, `users/${masterID}/areas/${area.id}/price-list`)
          );

          dbPriceList.docs.forEach((item) => {
            priceListData.push({
              areaID: area.id,
              procedureID: item.id,
              area: area.data().area,
              procedure: area.data().area + " | " + item.data().procedure,
              duration: item.data().hours * 60 + item.data().minutes,
              price: item.data().price,
            });
          });
          return priceListData;
        };

        let areasData = [];
        await Promise.all(
          areasSnapshot.docs.map(async (area) => {
            const priceList = await getPriceList(area);
            areasData.push(priceList);
          })
        ).then(() => {
          if (active) {
            setAreas(
              areasData.flat().sort((a, b) => a.procedure > b.procedure)
            );
          }
        });
      })();
    }

    return () => {
      active = false;
    };
  }, [areaLoading]);

  //LOADING TIME
  useEffect(() => {
    if (isDateSelected && selectedDate) {
      (async () => {
        setIsCounting(true);
        const q = query(
          collection(db, `users/${masterID}/schedule`),
          where("dayID", "==", new Date(selectedDate).getDay())
        );

        const dayData = await getDocs(q);

        let start = await dayData.docs[0].data().startWorkTime;

        let end = await dayData.docs[0].data().endWorkTime;

        setStartOfDayTime(
          start ? dateCercion(start.toDate(), selectedDate) : null
        );
        setEndOfDayTime(end ? dateCercion(end.toDate(), selectedDate) : null);
        setHasWorkTimeSet(true);
      })().then(() => {
        if (isDateSelected) {
          setIsDateSelected(false);
        }
      });
    }
  }, [isDateSelected]);

  useEffect(() => {
    if (!openAreas) {
      setAreas([]);
    }
  }, [openAreas]);

  useEffect(() => {
    let day = new Date(selectedDate).getDay();
    if (hasWorkTimeSet && !isNaN(day)) {
      if (startOfDayTime && endOfDayTime) {
        const getStartEndLunchTime = async () => {
          const q = query(
            collection(db, `users/${masterID}/schedule`),
            where("dayID", "==", day)
          );
          const dayData = await getDocs(q);

          let start = await dayData.docs[0].data().startLunchTime;
          let end = await dayData.docs[0].data().endLunchTime;

          setStartLunchTime(
            start ? dateCercion(start.toDate(), selectedDate) : null
          );
          setEndLunchTime(end ? dateCercion(end.toDate(), selectedDate) : null);
          setHasLunchTimeSet(true);
        };
        getStartEndLunchTime().then(() => {
          setHasWorkTimeSet(false);
        });
      } else {
        setTimeSlots(["В этот день у мастера выходной"]);
        if (hasWorkTimeSet) setHasWorkTimeSet(false);
        setIsCounting(false);
      }
    }
  }, [hasWorkTimeSet]);

  let getRegularRecords = () => {
    let proceduresTime = [];
    return new Promise(async (resolve) => {
      const dbRecords = query(
        collection(db, `users/${masterID}/records`),
        where("date", "==", dayjs(selectedDate).toISOString().split("T", 1)[0])
      );

      const recSnapshot = await getDocs(dbRecords);

      if (recSnapshot.docs.length > 0)
        recSnapshot.docs.forEach((rec) => {
          proceduresTime.push({
            start: dateCercion(rec.data().startTime.toDate(), selectedDate),
            end: dateCercion(rec.data().endTime.toDate(), selectedDate),
          });
        });
      else proceduresTime = [];

      resolve(proceduresTime);
    });
  };

  let getOnlineRecords = () => {
    let proceduresTime = [];
    return new Promise(async (resolve) => {
      const dbRecords = query(
        collection(db, `records`),
        where("date", "==", dayjs(selectedDate).toISOString().split("T", 1)[0])
      );

      const recSnapshot = await getDocs(dbRecords);
      if (recSnapshot.docs.length > 0)
        recSnapshot.docs.forEach((rec) => {
          proceduresTime.push({
            start: dateCercion(rec.data().startTime.toDate(), selectedDate),
            end: dateCercion(rec.data().endTime.toDate(), selectedDate),
          });
        });
      else proceduresTime = [];
      resolve(proceduresTime);
    });
  };

  useEffect(() => {
    const getRecordOnThisDate = async () => {
      if (hasLunchTimeSet) {
        startCalculatingFreeTime();
      }
    };

    getRecordOnThisDate();
  }, [hasLunchTimeSet]);

  const startCalculatingFreeTime = async () => {
    let regularRecords = getRegularRecords();
    let onlineRecords = getOnlineRecords();

    await Promise.all([regularRecords, onlineRecords])
      .then((one) => {
        if (startLunchTime && endLunchTime) {
          if (
            one[0]
              .concat(one[1])
              .concat({ start: startLunchTime, end: endLunchTime }).length > 0
          ) {
            calculateTimeBetweenRecords(
              one[0]
                .concat(one[1])
                .concat({ start: startLunchTime, end: endLunchTime })
            );
          } else {
            setAllTimesBetweenEntries([]);
          }
        } else {
          if (one[0].concat(one[1]).length > 0) {
            calculateTimeBetweenRecords(one[0].concat(one[1]));
          } else {
            setAllTimesBetweenEntries([]);
          }
        }
      })
      .then(() => {
        if (hasLunchTimeSet) setHasLunchTimeSet(false);
      });
  };

  const calculateTimeBetweenRecords = async (proceduresTimes) => {
    let betweenEntries = [];
    let start = undefined;
    let i = 0;
    proceduresTimes.sort((a, b) => a.start - b.start);
    //Определение начала времени на этот день
    if (
      startOfDayTime.toString().normalize() !==
      proceduresTimes[0].start.toString().normalize()
    ) {
      start = startOfDayTime;
      i = -1;
    } else {
      start = proceduresTimes[0].end;
    }
    //Рассчитать промежутки
    for (let j = i; j < proceduresTimes.length; j++) {
      if (proceduresTimes[j + 1] !== undefined) {
        if (start < proceduresTimes[j + 1].start) {
          betweenEntries.push({
            start: start,
            end: proceduresTimes[j + 1].start,
          });
        }
        start = proceduresTimes[j + 1].end;
      }
    }
    if (start < endOfDayTime)
      betweenEntries.push({
        start: start,
        end: endOfDayTime,
      });
    setAllTimesBetweenEntries(betweenEntries);
  };

  useEffect(() => {
    if (allTimesBetweenEntries) {
      let possibleTimes = [];
      if (allTimesBetweenEntries.length > 0) {
        allTimesBetweenEntries.forEach((t) => {
          if (
            new Date(t.start.getTime() + values.areaObject.duration * 60000) <=
            t.end
          )
            possibleTimes.push(t);
        });
      } else {
        possibleTimes.push({
          start: startOfDayTime,
          end: endOfDayTime,
        });
      }
      setPossibleTimesBetweenEntries(possibleTimes);
    }
  }, [allTimesBetweenEntries]);

  useEffect(() => {
    if (possibleTimesBetweenEntries) {
      // if the array contains one object with timestamps corresponding to the beginning and end of the working day
      if (possibleTimesBetweenEntries.length > 0) {
        let times = [];
        if (
          possibleTimesBetweenEntries.length === 1 &&
          startOfDayTime.toString().normalize() ===
            possibleTimesBetweenEntries[0].start.toString().normalize() &&
          endOfDayTime.toString().normalize() ===
            possibleTimesBetweenEntries[0].end.toString().normalize()
        ) {
          let startTime = startOfDayTime;
          let endTime = endOfDayTime;

          endTime.setMinutes(
            endOfDayTime.getMinutes() - values.areaObject.duration
          );
          while (startTime <= endTime) {
            let minutes = "0" + startTime.getMinutes();
            times.push(`${startTime.getHours()}:${minutes.slice(-2)}`);
            startTime.setMinutes(startTime.getMinutes() + smallestTimeInterval);
          }
        } else {
          possibleTimesBetweenEntries.forEach((t) => {
            let startTime = t.start;
            let endTime = t.end;
            endTime.setMinutes(
              endTime.getMinutes() - values.areaObject.duration
            );
            while (startTime <= endTime) {
              let minutes = "0" + t.start.getMinutes();
              times.push(`${startTime.getHours()}:${minutes.slice(-2)}`);

              startTime.setMinutes(
                startTime.getMinutes() + smallestTimeInterval
              );
            }
          });
        }
        setTimeSlots(times);
      } else {
        setTimeSlots([
          `Нет свободных мест для '${values.areaObject.procedure}`,
        ]);
      }
      setIsCounting(false);
    }
  }, [possibleTimesBetweenEntries]);

  useEffect(() => {
    if (areas.length > 0) {
      const sortAreas = areas.sort(function (a, b) {
        if (a.duration > b.duration) return 1;
        if (a.duration < b.duration) return -1;
        return 0;
      });

      setSmallestTimeInterval(sortAreas[0].duration);
    }
  }, [areas]);

  const handleCreateRecordInDatabase = async () => {
    let startTime = values.time.split(":");

    let start = selectedDate;
    let end = selectedDate;

    start.setHours(+startTime[0]);
    start.setMinutes(+startTime[1]);
    start.setSeconds(0);

    end = new Date(
      new Date(selectedDate).setMinutes(
        +startTime[1] + values.areaObject.duration
      )
    );
    setSubmitting(true);

    const color = randomColor();

    const newRecordRef = collection(db, `records`);
    await addDoc(newRecordRef, {
      masterID: masterID,
      clientID: !values.isNoCode ? values.clientCode : null,
      areaID: values.areaObject.areaID,
      fullName: !values.isNoCode ? null : values.fullName,
      phoneNumber: !values.isNoCode ? null : values.phone,
      color: color,
      procedureID: values.areaObject.procedureID,
      date: new Date(
        selectedDate.getTime() - selectedDate.getTimezoneOffset() * 60000
      )
        .toISOString()
        .split("T")[0],
      startTime: start,
      endTime: end,
      isDone: false,
      fromOnline: true,
      lastModified: new Date(),
    }).then((doc) => {
      setSubmitting(false);
      handleChange({
        target: {
          name: "clientCode",
          value: doc.id,
        },
      });
      nextStep();
    });
  };

  const onDateChange = async (selectedDate) => {
    if (selectedDate) {
      setTimeSlots([]);
      setSelectedDate(selectedDate);
      setIsDateSelected(true);
    }
  };
  return (
    <React.Fragment>
      <Autocomplete
        sx={{ mb: 5 }}
        className="w-100"
        open={openAreas}
        onOpen={() => {
          setOpenAreas(true);
        }}
        onClose={() => {
          setOpenAreas(false);
        }}
        isOptionEqualToValue={(option, value) =>
          option.procedure == value.procedure
        }
        getOptionLabel={(option) =>
          option.procedure || values.areaObject.procedure
        }
        options={areas}
        noOptionsText="Нет вариантов"
        loading={areaLoading}
        loadingText="Загрузка..."
        value={values.areaObject}
        onChange={(event, newValue) => {
          handleChange({
            target: {
              name: "areaObject",
              value: newValue,
            },
          });
          handleChange({
            target: {
              name: "time",
              value: "",
            },
          });
        }}
        renderInput={(params) => (
          <TextField
            {...params}
            label="Услуга"
            variant="standard"
            InputProps={{
              ...params.InputProps,
              readOnly: true,
              endAdornment: (
                <React.Fragment>
                  {areaLoading ? (
                    <CircularProgress color="inherit" size={20} />
                  ) : null}
                  {params.InputProps.endAdornment}
                </React.Fragment>
              ),
            }}
          />
        )}
      />
      <div className="w-100 datePickerContainer">
        <DatePicker
          date={selectedDate}
          onDateChange={(date) => {
            handleChange({
              target: {
                name: "date",
                value: date,
              },
            });
            handleChange({
              target: {
                name: "time",
                value: "",
              },
            });

            onDateChange(date);
          }}
          locale={ru}
          modifiers={modifiers}
          modifiersClassNames={modifiersClassNames}
          minimumDate={new Date()}
        >
          {({ inputProps, focused }) => (
            <TextField
              inputProps={{
                ...inputProps,
              }}
              InputProps={{
                readOnly: true,
              }}
              disabled={isDateDisabled}
              className={"w-100" + (focused ? " -focused" : "")}
              id="date"
              autoComplete="off"
              label="Дата"
              variant="standard"
            />
          )}
        </DatePicker>
      </div>
      <Autocomplete
        className="w-100"
        open={openTimeSlots}
        onOpen={() => {
          setOpenTimeSlots(true);
        }}
        onClose={() => {
          setOpenTimeSlots(false);
        }}
        getOptionDisabled={(option) =>
          option.startsWith("В этот день") ||
          option.startsWith("Нет свободных мест")
        }
        isOptionEqualToValue={(option, value) => option.time == value.time}
        getOptionLabel={(time) => time || values.time}
        options={timeSlots}
        noOptionsText="Нет вариантов"
        loading={timeLoading}
        value={values.time}
        loadingText="Минутку... Загружаются данные"
        defaultValue={values.time}
        onChange={(event, newValue) => {
          handleChange({
            target: {
              name: "time",
              value: newValue,
            },
          });
        }}
        renderInput={(params) => (
          <TextField
            {...params}
            label="Время"
            variant="standard"
            InputProps={{
              ...params.InputProps,
              readOnly: true,
              endAdornment: (
                <React.Fragment>
                  {timeLoading ? (
                    <CircularProgress color="inherit" size={20} />
                  ) : null}
                  {params.InputProps.endAdornment}
                </React.Fragment>
              ),
            }}
          />
        )}
      />
      <div
        style={{
          width: " 100%",
          display: "grid",
          gridTemplateColumns: "1fr 1fr",
          gridGap: "10px",
        }}
      >
        <button className="button" onClick={back}>
          Назад
        </button>
        <button disabled={disabled} className="button" onClick={Continue}>
          Подтвердить
        </button>
      </div>
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={submitting}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </React.Fragment>
  );
}
