import React, { useState, useEffect } from "react";
import {
  TextField,
  FormGroup,
  FormControlLabel,
  Checkbox,
} from "@mui/material";
import { doc, getDoc } from "firebase/firestore";

import "../styles/style.css";
import colors from "../styles/colors";

export default function FormUserClientCode({
  values,
  handleChange,
  nextStep,
  db,
  masterID,
}) {
  const [disabled, setDisabled] = useState(true);
  const [disabledCheckbox, setDisabledCheckbox] = useState(
    values.clientCode.length === 20 && !values.isNoCode
  );
  const [dynamicHelperText, setDynamicHelperText] = useState("");
  const [checkboxClicked, setCheckboxClicked] = useState(values.isNoCode);

  const Continue = (e) => {
    e.preventDefault();
    const stepParams = checkboxClicked ? 1 : 2;
    handleChange({
      target: {
        name: "isNoCode",
        value: checkboxClicked,
      },
    });
    nextStep(stepParams);
  };

  useEffect(() => {
    setDisabled(!checkboxClicked);
  }, [checkboxClicked]);

  useEffect(() => {
    if (
      (values.clientCode.length === 20 && values.isNoCode) ||
      (values.clientCode.length === 20 && !values.isNoCode) ||
      (values.clientCode.length < 20 && values.isNoCode)
    ) {
      setDisabled(false);
    } else {
      setDisabled(true);
    }
  }, []);

  useEffect(() => {
    if (values.clientCode.length < 20 && !values.isNoCode) {
      setDisabled(true);
    }
  }, [values.clientCode]);

  const handleCodeChange = async (event) => {
    handleChange(event);
    setCheckboxClicked(false);
    setDisabledCheckbox(false);

    if (event.target.value.length === 20) {
      setDynamicHelperText("Подождите... Выполняется запрос");
      const clientRef = doc(
        db,
        `users/${masterID}/clients/${event.target.value}`
      );

      const clientSnap = await getDoc(clientRef);

      if (clientSnap.exists()) {
        handleChange({
          target: {
            name: "fullName",
            value: clientSnap.data().name,
          },
        });
        handleChange({
          target: {
            name: "phone",
            value: clientSnap.data().phoneNumber,
          },
        });
        setDynamicHelperText("");
        setDisabled(false);
        setDisabledCheckbox(true);
      } else {
        setDynamicHelperText("К сожалению, мы не можем вас найти");
        setDisabled(true);
      }
    } else {
      setDynamicHelperText("");
    }
  };

  return (
    <React.Fragment>
      <TextField
        placeholder="20-значный код клиента"
        label="Ваш код"
        onChange={handleCodeChange}
        defaultValue={values.clientCode}
        name="clientCode"
        variant="standard"
        className="w-100"
        sx={{ mb: 5 }}
        inputProps={{
          maxLength: 20,
        }}
        helperText={dynamicHelperText}
      />
      <FormGroup className="w-100">
        <FormControlLabel
          control={
            <Checkbox
              disabled={disabledCheckbox}
              checked={checkboxClicked}
              style={{
                color: disabledCheckbox ? "rgba(0,0,0,0.4)" : styles.color,
              }}
              onClick={() => setCheckboxClicked(!checkboxClicked)}
            />
          }
          label={`Я уверен(а), что не записывалась к этому мастеру онлайн`}
          color={styles.color}
        />
      </FormGroup>
      <button disabled={disabled} className="button" onClick={Continue}>
        Продолжить
      </button>
    </React.Fragment>
  );
}
const styles = {
  color: colors.main,
};
