import React, { useState, useEffect } from "react";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import InputMask from "react-input-mask";

import "../styles/style.css";

export default function FormUserDetails({
  values,
  handleChange,
  prevStep,
  nextStep,
}) {
  const [disabled, setDisabled] = useState(true);
  const [error, setError] = useState(false);

  const Continue = (e) => {
    e.preventDefault();
    nextStep();
  };

  const back = (e) => {
    e.preventDefault();
    prevStep();
  };

  useEffect(() => {
    if (
      values.fullName.length >= 2 &&
      values.phone.replace(/-|_/g, "").length === 14
    ) {
      setDisabled(false);
    } else {
      setDisabled(true);
    }
  }, [values.phone, values.fullName]);

  return (
    <React.Fragment>
      <TextField
        error={error}
        helperText={error && "В имени должно быть по меньшей мере две буквы"}
        label="Полное имя"
        onChange={(event) => handleChange(event)}
        onBlur={() => {
          if (values.fullName.length < 2) setError(true);
          else setError(false);
        }}
        defaultValue={values.fullName}
        name="fullName"
        variant="standard"
        className="w-100"
        sx={{ mb: 5 }}
      />
      <InputMask
        mask="(999) 999 9999"
        value={values.phone}
        onChange={(event) => handleChange(event)}
      >
        {() => (
          <TextField
            variant="standard"
            label="Номер"
            name="phone"
            InputProps={{
              type: "tel",
              inputMode: "numeric",
            }}
            className="w-100"
          />
        )}
      </InputMask>
      <div
        style={{
          width: " 100%",
          display: "grid",
          gridTemplateColumns: "1fr 1fr",
          gridGap: "10px",
        }}
      >
        <button className="button" onClick={back}>
          Назад
        </button>
        <button disabled={disabled} className="button" onClick={Continue}>
          Продолжить
        </button>
      </div>
    </React.Fragment>
  );
}
