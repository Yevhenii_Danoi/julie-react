import React, { useEffect, useState } from "react";
import { List, ListItemText, TextField } from "@mui/material";
import colors from "../styles/colors";
import "../styles/style.css";
import { CopyToClipboard } from "react-copy-to-clipboard";

export default function Success({ values }) {
  const { fullName, phone, time, date, areaObject, isNoCode, clientCode } =
    values;

  const [copying, setCopying] = useState(false);

  const copy = () => {
    setCopying(true);
  };

  useEffect(() => {
    if (copying) {
      navigator.clipboard.writeText(clientCode);
      setTimeout(() => {
        setCopying(false);
      }, 2000);
    }
  }, [copying]);
  return (
    <React.Fragment>
      {isNoCode && (
        <>
          <p>
            Пожалуйста, не теряйте этот код и никому его не передавайте.
            Используйте его при следующей онлайн-записи. Если вы потеряете код,
            скажите об этом мастеру
          </p>
          <div
            style={{
              width: " 100%",
              display: "grid",
              gridTemplateColumns: "3fr 1fr",
              gridGap: "10px",
            }}
          >
            <TextField
              disabled
              label="Ваш уникальный код"
              variant="outlined"
              defaultValue={clientCode}
              name="clientCode"
              className="w-100"
            />
            <button
              className="button copyBtn"
              style={{
                backgroundColor: copying ? "#dbffdb" : "rgb(23, 28, 33)",
                color: copying ? "#171c21" : "rgb(255, 255, 255)",
              }}
              onClick={copy}
            >
              {copying ? "Скопировано" : "Копировать"}
            </button>
          </div>
        </>
      )}
      <h1 style={styles.h1}>Спасибо за вашу запись</h1>
      <List>
        <ListItemText primary={`Имя: ${fullName}`} />
        <ListItemText primary={`Номер телефона: ${phone}`} />
        <ListItemText primary={`Время: ${time}`} />
        <ListItemText primary={`Дата: ${date}`} />
        <ListItemText
          primary={`Услуга: ${areaObject && areaObject.procedure}`}
        />
      </List>
    </React.Fragment>
  );
}
const styles = {
  h1: {
    color: colors.main,
  },
};
