import React from "react";

import colors from "../styles/colors";

export default function NotFound() {
  return (
    <React.Fragment>
      <h1 style={styles.h1}>Упс... Похоже у вас неверная ссылка</h1>
    </React.Fragment>
  );
}

const styles = {
  h1: {
    color: colors.main,
  },
};
