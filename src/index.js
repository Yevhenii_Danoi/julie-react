import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import "./styles/style.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import reportWebVitals from "./reportWebVitals";

import HomePage from "./components/HomePage";
import RecordForm from "./components/RecordForm";
import NotFound from "./components/NotFound";
import PrivacyPolicy from "./components/PrivacyPolicy";

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route
          path="/create-record/:masterId"
          element={<HomePage content={<RecordForm />} />}
        />
        <Route path="/privacy-policy/" element={<PrivacyPolicy />} />
        <Route path="*" element={<HomePage content={<NotFound />} />} />
      </Routes>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
